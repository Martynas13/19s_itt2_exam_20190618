#!/usr/bin/env python3

import yaml
import os

conf_file = "user.yml"

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        cfg = yaml.safe_load(stream)

    try:
        print( "'first_name': First name is a string", str(cfg['first_name'] ) )
        print( "'user_url': user_url is a string", str(cfg['user_url'] ) )
        if not cfg['user_url'].startswith('https://gitlab.com'):
                raise ValueError("user_url not pointing to gitlab.com: '%s'"%cfg['user_url'])
        print( "'user_url': user_url points to gitlab.com", str(cfg['user_url'] ) )

    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)

